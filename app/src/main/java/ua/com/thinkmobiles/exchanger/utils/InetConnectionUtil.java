package ua.com.thinkmobiles.exchanger.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import ua.com.thinkmobiles.exchanger.activities.MainActivity;

/**
 * Created by Admin on 19.09.2015.
 */
public class InetConnectionUtil {
    private InetConnectionUtil() {}

    public static boolean connectionTest(final Context _context) {
        boolean connection = false;

        ConnectivityManager manager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()) {
            connection = true;
            Log.d(MainActivity.TAG, "connection true");
        }
        Log.d(MainActivity.TAG, "connection false");
        return connection;
    }
}
