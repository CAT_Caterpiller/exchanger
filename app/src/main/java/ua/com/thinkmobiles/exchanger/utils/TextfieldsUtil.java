package ua.com.thinkmobiles.exchanger.utils;

import android.text.TextUtils;

/**
 * Created by Admin on 19.09.2015.
 */
public class TextfieldsUtil {
    private TextfieldsUtil(){}

    public static String getPhoneField(String phone) {
        if(TextUtils.isEmpty(phone)) {
            return "Тел: " + " " + "-----";
        } else {
            return "Тел: " + " " + phone;
        }
    }

    public static String getAddressField(String address) {
        if(TextUtils.isEmpty(address)) {
            return "Адрес: " + " " + "-----";
        } else {
            return "Адрес: " + " " + address;
        }
    }
}
