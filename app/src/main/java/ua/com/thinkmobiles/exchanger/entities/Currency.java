package ua.com.thinkmobiles.exchanger.entities;

/**
 * Created by Admin on 14.09.2015.
 */
public class Currency {
    private String mAbbr;
    private String mName;

    public Currency() {}

    public Currency(String _abbr, String _name) {
        mAbbr = _abbr;
        mName = _name;
    }

    public String getAbbr() {
        return mAbbr;
    }

    public void setAbbr(String _abbr) {
        mAbbr = _abbr;
    }

    public String getName() {
        return mName;
    }

    public void setName(String _name) {
        mName = _name;
    }

}
