package ua.com.thinkmobiles.exchanger.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ua.com.thinkmobiles.exchanger.entities.City;
import ua.com.thinkmobiles.exchanger.entities.Course;
import ua.com.thinkmobiles.exchanger.entities.Currency;
import ua.com.thinkmobiles.exchanger.entities.Organization;
import ua.com.thinkmobiles.exchanger.entities.OrganizationType;
import ua.com.thinkmobiles.exchanger.entities.Region;

/**
 * Created by Admin on 17.09.2015.
 */
public class DbReader {
    private DbOpenHelper mDbOpenHelper;
    private Context mContext;

    public DbReader(Context _context) {
        mContext = _context;
        mDbOpenHelper = DbOpenHelper.getInstance(_context);
    }

    public boolean checkDataBase() {
        File dbFile = mContext.getDatabasePath(DbOpenHelper.DB_NAME);
        return dbFile.exists();
    }

    public List<Organization> readDataOrganizations() {
        SQLiteDatabase db = mDbOpenHelper.getReadableDatabase();
        List<Organization> list = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DbOpenHelper.ORGANIZATION_TABLE, null);
        int indId = cursor.getColumnIndex(DbOpenHelper.ID);
        int indIdKey = cursor.getColumnIndex(DbOpenHelper.ID_KEY);
        int indOldId = cursor.getColumnIndex(DbOpenHelper.OLDID_KEY);
        int indOrgType = cursor.getColumnIndex(DbOpenHelper.ORGTYPE_KEY);
        int indTitle = cursor.getColumnIndex(DbOpenHelper.TITLE_KEY);
        int indRegion = cursor.getColumnIndex(DbOpenHelper.REGION_KEY);
        int indCity = cursor.getColumnIndex(DbOpenHelper.CITY_KEY);
        int indPhone = cursor.getColumnIndex(DbOpenHelper.PHONE_KEY);
        int indAddress = cursor.getColumnIndex(DbOpenHelper.ADDRESS_KEY);
        int indLink = cursor.getColumnIndex(DbOpenHelper.LINK_KEY);
        cursor.moveToFirst();
        while (cursor.moveToNext()) {
            Organization organization = new Organization();
            organization.setId(cursor.getInt(indId));
            organization.setOrgId(cursor.getString(indIdKey));
            organization.setOldId(cursor.getString(indOldId));
            organization.setOrgType(getType(db, cursor.getString(indOrgType)));
            organization.setTitle(cursor.getString(indTitle));
            organization.setRegion(getRegion(db, cursor.getString(indRegion)));
            organization.setCity(getCity(db, cursor.getString(indCity)));
            organization.setPhone(cursor.getString(indPhone));
            organization.setAddress(cursor.getString(indAddress));
            organization.setLink(cursor.getString(indLink));
            organization.setCourses(getCourses(db, cursor.getString(indIdKey)));            
            list.add(organization);
        }
        cursor.close();
        return list;
    }

    public Organization readOrganization(String id) {
        SQLiteDatabase db = mDbOpenHelper.getReadableDatabase();
        Organization organization = new Organization();
        Cursor cursor = db.query(DbOpenHelper.ORGANIZATION_TABLE, null, "id_key=?", new String[]{id}, null, null, null);
        try {
            if(cursor.moveToNext()) {
                organization.setId(cursor.getInt(cursor.getColumnIndexOrThrow(DbOpenHelper.ID)));
                organization.setOrgId(id);
                organization.setOrgId(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.OLDID_KEY)));
                organization.setOrgType(getType(db, cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.ORGTYPE_KEY))));
                organization.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.TITLE_KEY)));
                organization.setRegion(getRegion(db, cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.REGION_KEY))));
                organization.setCity(getCity(db, cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.CITY_KEY))));
                organization.setPhone(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.PHONE_KEY)));
                organization.setAddress(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.ADDRESS_KEY)));
                organization.setLink(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.LINK_KEY)));
                organization.setCourses(getCourses(db, id));
            }
        } finally {
            cursor.close();
        }
        return organization;
    }

    private OrganizationType getType(SQLiteDatabase db, String key) {
        Cursor cursor = db.query(DbOpenHelper.ORGTYPE_TABLE, null, "_id=?", new String[]{key}, null, null, null);
        try {
            if (cursor.moveToNext()) {
                OrganizationType type = new OrganizationType();
                type.setId(key);
                type.setName(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.VALUE_KEY)));
                return type;
            } else {
                return null;
            }
        } finally {
            cursor.close();
        }
    }

    private Region getRegion(SQLiteDatabase db, String key) {
        Cursor cursor = db.query(DbOpenHelper.REGION_TABLE, null, "_id=?", new String[]{key}, null, null, null);
        try {
            if (cursor.moveToNext()) {
                Region region = new Region();
                region.setId(key);
                region.setName(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.VALUE_KEY)));
                return region;
            } else {
                return null;
            }
        } finally {
            cursor.close();
        }
    }

    private City getCity(SQLiteDatabase db, String key) {
        Cursor cursor = db.query(DbOpenHelper.CITY_TABLE, null, "_id=?", new String[]{key}, null, null, null);
        try {
            if (cursor.moveToNext()) {
                City city = new City();
                city.setId(key);
                city.setName(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.VALUE_KEY)));
                return city;
            } else {
                return null;
            }
        } finally {
            cursor.close();
        }
    }

    private List<Course> getCourses(SQLiteDatabase db, String org_key) {
        List<Course> courses = new ArrayList<>();

        Cursor cursor = db.query(DbOpenHelper.COURSE_TABLE, null, DbOpenHelper.ORGANIZATION_KEY + "=?",
                new String[]{org_key}, null, null, null);

        try {
            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                Course course = new Course();
                course.setCurrency(getCurrency(db, cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.CURRENCY_KEY))));
                course.setAsk(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.ASK)));
                course.setBid(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.BID)));
                course.setAskFluctuation(cursor.getInt(cursor.getColumnIndexOrThrow(DbOpenHelper.FLUCTUATION_ASK)));
                course.setBidFluctuation(cursor.getInt(cursor.getColumnIndexOrThrow(DbOpenHelper.FLUCTUATION_BID)));
                courses.add(course);
            }
        } finally {
            cursor.close();
        }
        return courses;
    }

    private Currency getCurrency(SQLiteDatabase db, String key) {
        Cursor cursor = db.query(DbOpenHelper.CURRENCY_TABLE, null, "_id=?", new String[]{key}, null, null, null);
        try {
            if (cursor.moveToNext()) {
                Currency currency = new Currency();
                currency.setAbbr(key);
                currency.setName(cursor.getString(cursor.getColumnIndexOrThrow(DbOpenHelper.VALUE_KEY)));
                return currency;
            } else {
                return null;
            }
        } finally {
            cursor.close();
        }
    }

}
