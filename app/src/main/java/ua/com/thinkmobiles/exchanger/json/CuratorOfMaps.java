package ua.com.thinkmobiles.exchanger.json;

import java.util.Map;

/**
 * Created by Admin on 14.09.2015.
 */
public class CuratorOfMaps {
    private String mDate;
    private Map<String, String> mAllTypes;
    private Map<String, String> mAllCurrencies;
    private Map<String, String> mAllRegions;
    private Map<String, String> mAllCities;

    public String getDate() {
        return mDate;
    }

    public void setDate(String _date) {
        mDate = _date;
    }

    public Map<String, String> getAllTypes() {
        return mAllTypes;
    }

    public void setAllTypes(Map<String, String> _allTypes) {
        mAllTypes = _allTypes;
    }

    public Map<String, String> getAllCurrencies() {
        return mAllCurrencies;
    }

    public void setAllCurrencies(Map<String, String> _allCurrencies) {
        mAllCurrencies = _allCurrencies;
    }

    public Map<String, String> getAllRegions() {
        return mAllRegions;
    }

    public void setAllRegions(Map<String, String> _allRegions) {
        mAllRegions = _allRegions;
    }

    public Map<String, String> getAllCities() {
        return mAllCities;
    }

    public void setAllCities(Map<String, String> _allCities) {
        mAllCities = _allCities;
    }
}
