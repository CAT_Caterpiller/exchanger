package ua.com.thinkmobiles.exchanger.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.concurrent.ExecutionException;

import ua.com.thinkmobiles.exchanger.R;
import ua.com.thinkmobiles.exchanger.utils.InetConnectionUtil;
import ua.com.thinkmobiles.exchanger.json.MapLoader;
import ua.com.thinkmobiles.exchanger.utils.TabClickUtil;

/**
 * Created by Admin on 19.09.2015.
 */
public class MapActivity extends Activity implements OnMapReadyCallback {
    private String mOrganization;
    private String mCity;
    private String mAddress;
    private GoogleMap mGoogleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent intent = getIntent();
        mOrganization = intent.getStringExtra(TabClickUtil.ORGANIZATION);
        mCity = intent.getStringExtra(TabClickUtil.CITY);
        mAddress = intent.getStringExtra(TabClickUtil.ADDRESS);

        if(InetConnectionUtil.connectionTest(this)) {
            MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment_MAP);
            mapFragment.getMapAsync(this);
        } else {
            Toast.makeText(this, "not Internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap _googleMap) {
        MapLoader mapLoader = new MapLoader();
        mapLoader.execute(mCity, mAddress);
        mGoogleMap = _googleMap;
        if (mGoogleMap == null) return;
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        try {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mapLoader.get(), mapLoader.getZoom()));
            mGoogleMap.addMarker(new MarkerOptions()
                    .position(mapLoader.get())
                    .title(mOrganization)
                    .snippet(mAddress));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
