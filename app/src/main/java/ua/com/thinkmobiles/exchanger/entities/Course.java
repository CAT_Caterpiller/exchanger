package ua.com.thinkmobiles.exchanger.entities;

/**
 * Created by Admin on 15.09.2015.
 */
public class Course {
    private Currency mCurrency;
    private String ask;
    private String bid;
    private int askFluctuation;
    private int bidFluctuation;

    public Currency getCurrency() {
        return mCurrency;
    }

    public void setCurrency(Currency _currency) {
        mCurrency = _currency;
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String _ask) {
        ask = _ask;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String _bid) {
        bid = _bid;
    }

    public int getAskFluctuation() {
        return askFluctuation;
    }

    public void setAskFluctuation(int _askFluctuation) {
        askFluctuation = _askFluctuation;
    }

    public int getBidFluctuation() {
        return bidFluctuation;
    }

    public void setBidFluctuation(int _bidFluctuation) {
        bidFluctuation = _bidFluctuation;
    }
}
