package ua.com.thinkmobiles.exchanger.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import ua.com.thinkmobiles.exchanger.receivers.AlarmReceiver;

/**
 * Created by Admin on 17.09.2015.
 */
public class AlarmUtil {
    private final static long INTEVAL = 30 * 60 *  1000;

    private AlarmUtil() {}

    public static void startAlert(Context _context) {
        Intent intent = new Intent(_context, AlarmReceiver.class);
        boolean alarmRunning = (PendingIntent.getBroadcast(_context.getApplicationContext(), 0, intent, PendingIntent.FLAG_NO_CREATE) != null);
        if(alarmRunning == false) {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(_context.getApplicationContext(), 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager) _context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime(), INTEVAL, pendingIntent);
        }
    }
}
