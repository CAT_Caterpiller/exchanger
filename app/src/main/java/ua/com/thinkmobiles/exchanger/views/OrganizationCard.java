package ua.com.thinkmobiles.exchanger.views;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import ua.com.thinkmobiles.exchanger.R;

/**
 * Created by Admin on 18.09.2015.
 */
public class OrganizationCard extends LinearLayout {
    private Context mContext;
    private TextView mTitle, mRegion, mCity, mPhone, mAddress;
    private TabLayout mTabLayout;

    public OrganizationCard(Context _context) {
        super(_context);
        mContext = _context;
        initFields();
    }

    public OrganizationCard(Context _context, AttributeSet _attrs) {
        super(_context, _attrs);
        mContext = _context;
        initFields();
    }

    public OrganizationCard(Context _context, AttributeSet _attrs, int defStyleAttrs) {
        super(_context, _attrs, defStyleAttrs);
        mContext = _context;
        initFields();
    }

    private void initFields() {
        inflate(mContext, R.layout.organiz_card, this);
        mTitle = (TextView) findViewById(R.id.tvOrgTitle_MA);
        mRegion = (TextView) findViewById(R.id.tvRegion_MA);
        mCity = (TextView) findViewById(R.id.tvCity_MA);
        mPhone = (TextView) findViewById(R.id.tvPhone_MA);
        mAddress = (TextView) findViewById(R.id.tvAddress_MA);
        mTabLayout = (TabLayout) findViewById(R.id.tabLayout_MA);
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(new CustomTabView(mContext, R.drawable.ic_link)));
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(new CustomTabView(mContext, R.drawable.ic_map)));
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(new CustomTabView(mContext, R.drawable.ic_phone)));
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(new CustomTabView(mContext, R.drawable.ic_next)));
    }

    public TextView getTitle() {
        return mTitle;
    }

    public void setTitle(TextView _title) {
        mTitle = _title;
    }

    public TextView getRegion() {
        return mRegion;
    }

    public void setRegion(TextView _region) {
        mRegion = _region;
    }

    public TextView getCity() {
        return mCity;
    }

    public void setCity(TextView _city) {
        mCity = _city;
    }

    public TextView getPhone() {
        return mPhone;
    }

    public void setPhone(TextView _phone) {
        mPhone = _phone;
    }

    public TextView getAddress() {
        return mAddress;
    }

    public void setAddress(TextView _address) {
        mAddress = _address;
    }

    public TabLayout getTabLayout() {
        return mTabLayout;
    }

    public void setTabLayout(TabLayout _tabLayout) {
        mTabLayout = _tabLayout;
    }
}
