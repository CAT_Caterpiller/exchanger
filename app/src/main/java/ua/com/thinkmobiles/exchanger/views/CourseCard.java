package ua.com.thinkmobiles.exchanger.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ua.com.thinkmobiles.exchanger.R;
import ua.com.thinkmobiles.exchanger.entities.Course;

/**
 * Created by Admin on 20.09.2015.
 */
public class CourseCard extends LinearLayout {
    private Context mContext;
    private TextView mTitle, mAsk, mBid;
    private ImageView mImageAsk, mImageBid;

    public CourseCard(Context _context) {
        super(_context);
        mContext = _context;
        initFields();
    }

    public CourseCard(Context _context, AttributeSet _attrs) {
        super(_context, _attrs);
        mContext = _context;
        initFields();
    }

    public CourseCard(Context _context, AttributeSet _attrs, int defStyleAttrs) {
        super(_context, _attrs, defStyleAttrs);
        mContext = _context;
        initFields();
    }

    public void createCurrencyCard(Course _course) {
        Log.d("MMM", _course.getCurrency().getName());
        mTitle.setText(_course.getCurrency().getName());
        mAsk.setText(_course.getAsk());
        mBid.setText(_course.getBid());
        if(_course.getAskFluctuation() == 1) {
            mAsk.setTextColor(mContext.getResources().getColor(R.color.green_marker));
            mImageAsk.setImageResource(R.drawable.ic_green_arrow_up);
        } else {
            mAsk.setTextColor(mContext.getResources().getColor(R.color.red_marker));
            mImageAsk.setImageResource(R.drawable.ic_red_arrow_down);
        }
        if(_course.getBidFluctuation() == 1) {
            mBid.setTextColor(mContext.getResources().getColor(R.color.green_marker));
            mImageBid.setImageResource(R.drawable.ic_green_arrow_up);
        } else {
            mBid.setTextColor(mContext.getResources().getColor(R.color.red_marker));
            mImageBid.setImageResource(R.drawable.ic_red_arrow_down);
        }
    }

    private void initFields() {
        inflate(mContext, R.layout.list_cuurency, this);
        mTitle = (TextView) findViewById(R.id.tvCurrency_OA);
        mAsk = (TextView) findViewById(R.id.tvAsk_OA);
        mBid = (TextView) findViewById(R.id.tvBid_OA);
        mImageAsk = (ImageView) findViewById(R.id.iconAsk_OA);
        mImageBid = (ImageView) findViewById(R.id.iconBid_OA);
    }

    public TextView getTitle() {
        return mTitle;
    }

    public void setTitle(TextView _title) {
        mTitle = _title;
    }

    public TextView getAsk() {
        return mAsk;
    }

    public void setAsk(TextView _ask) {
        mAsk = _ask;
    }

    public TextView getBid() {
        return mBid;
    }

    public void setBid(TextView _bid) {
        mBid = _bid;
    }

    public ImageView getImageAsk() {
        return mImageAsk;
    }

    public void setImageAsk(ImageView _imageAsk) {
        mImageAsk = _imageAsk;
    }

    public ImageView getImageBid() {
        return mImageBid;
    }

    public void setImageBid(ImageView _imageBid) {
        mImageBid = _imageBid;
    }
}
