package ua.com.thinkmobiles.exchanger.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import ua.com.thinkmobiles.exchanger.R;

/**
 * Created by Admin on 18.09.2015.
 */
public class CustomTabView extends LinearLayout {    
    private ImageView mImageView;

    public CustomTabView(Context context, int resours) {
        super(context);
        inflate(context, R.layout.tab_image_layout, this);
        getBackground(resours);
    }

    public CustomTabView(Context context, AttributeSet attrs, int resours) {
        super(context, attrs);
        inflate(context, R.layout.tab_image_layout, this);
        getBackground(resours);
    }

    public CustomTabView(Context context, AttributeSet attrs, int defStyleAttr, int resours) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.tab_image_layout, this);
        getBackground(resours);
    }

    private void getBackground(int resours) {
        mImageView = (ImageView) findViewById(R.id.tabImage_MA);
        mImageView.setImageResource(resours);
    }
}
