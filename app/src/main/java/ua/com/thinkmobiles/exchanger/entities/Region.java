package ua.com.thinkmobiles.exchanger.entities;

/**
 * Created by Admin on 14.09.2015.
 */
public class Region {
    private String mId;
    private String mName;

    public Region() {
    }

    public Region(String _id, String _name) {
        mId = _id;
        mName = _name;
    }

    public String getId() {
        return mId;
    }

    public void setId(String _id) {
        mId = _id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String _name) {
        mName = _name;
    }
}
