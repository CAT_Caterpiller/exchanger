package ua.com.thinkmobiles.exchanger.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import ua.com.thinkmobiles.exchanger.entities.Course;
import ua.com.thinkmobiles.exchanger.json.CuratorOfMaps;
import ua.com.thinkmobiles.exchanger.json.JsonLoader;
import ua.com.thinkmobiles.exchanger.json.OrganizationJson;
import ua.com.thinkmobiles.exchanger.services.AlarmService;
import ua.com.thinkmobiles.exchanger.utils.InetConnectionUtil;

/**
 * Created by Admin on 15.09.2015.
 */
public class DbCurator {
    private Context mContext;
    private DbOpenHelper mDbOpenHelper;
    private JsonLoader mJsonLoader;

    private static final String APP_PREF = "app_pref";
    private static final String DATE_FLAG = "date_flag";

    public DbCurator(Context _context) {
        mContext = _context;
        mDbOpenHelper = DbOpenHelper.getInstance(_context);
        mJsonLoader = new JsonLoader(_context);
    }

    public void performAllOperations() {
        if(InetConnectionUtil.connectionTest(mContext)) {
            try {
                mJsonLoader.receivingJson();
                if(mJsonLoader.checkJson()) {
                    updateDataBase();
                    if(checkUpdatesJson(mJsonLoader.getDate())) {
                        updateDataBase();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        stopAlarmService();
    }

    private boolean checkUpdatesJson(String _date) {
        SharedPreferences preferences = mContext.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;
        if(!preferences.getString(DATE_FLAG, "").isEmpty() && preferences.getString(DATE_FLAG, "").equals(_date)) {
            return false;
        } else {
            editor = preferences.edit();
            editor.putString(DATE_FLAG, _date);
            editor.apply();
            return true;
        }
    }

    private void updateDataBase() {
        try {
            saveAllObjects();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveAllObjects() throws JSONException {
        CuratorOfMaps curatorOfMaps = mJsonLoader.getAllMaps();
        saveObjects(curatorOfMaps.getAllCities(), DbOpenHelper.CITY_TABLE);
        saveObjects(curatorOfMaps.getAllRegions(), DbOpenHelper.REGION_TABLE);
        saveObjects(curatorOfMaps.getAllTypes(), DbOpenHelper.ORGTYPE_TABLE);
        saveObjects(curatorOfMaps.getAllCurrencies(), DbOpenHelper.CURRENCY_TABLE);
        saveOrganizations(mJsonLoader.getAllOrganizationsJson(mJsonLoader.getAllMaps()));
        saveCourses(mJsonLoader.getAllOrganizationsJson(mJsonLoader.getAllMaps()));
    }

    private void saveObjects(Map<String, String> _map, String _table) {
        SQLiteDatabase db = mDbOpenHelper.getWritableDatabase();
        db.delete(_table, null, null);
        for (Map.Entry<String, String> entry : _map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            ContentValues values = new ContentValues();
            values.put(DbOpenHelper.ID, key);
            values.put(DbOpenHelper.VALUE_KEY, value);
            db.insert(_table, null, values);
        }
        db.close();
    }

    private void saveOrganizations(List<OrganizationJson> _list) {
        SQLiteDatabase db = mDbOpenHelper.getWritableDatabase();
        db.delete(DbOpenHelper.ORGANIZATION_TABLE, null, null);
        for (OrganizationJson org : _list) {
            ContentValues values = new ContentValues();
            values.put(DbOpenHelper.ID_KEY, org.getId());
            values.put(DbOpenHelper.OLDID_KEY, org.getOldId());
            values.put(DbOpenHelper.ORGTYPE_KEY, org.getOrgType());
            values.put(DbOpenHelper.TITLE_KEY, org.getTitle());
            values.put(DbOpenHelper.PHONE_KEY, org.getPhone());
            values.put(DbOpenHelper.ADDRESS_KEY, org.getAddress());
            values.put(DbOpenHelper.LINK_KEY, org.getLink());
            values.put(DbOpenHelper.REGION_KEY, org.getRegionId());
            values.put(DbOpenHelper.CITY_KEY, org.getCityId());
            db.insert(DbOpenHelper.ORGANIZATION_TABLE, null, values);
        }
        db.close();
    }

    private void saveCourses(List<OrganizationJson> _list) {
        SQLiteDatabase db = mDbOpenHelper.getWritableDatabase();
        for (OrganizationJson org : _list) {
            for (Course c : org.getCourses()) {
                Cursor cursor = db.query(DbOpenHelper.COURSE_TABLE, new String[]{DbOpenHelper.ASK, DbOpenHelper.BID},
                        DbOpenHelper.CURRENCY_KEY + "=? AND " + DbOpenHelper.ORGANIZATION_KEY + "=?", new String[]
                                {c.getCurrency().getAbbr(), org.getId()}, null, null, null);

                try {
                    if (cursor.moveToNext()) {
                        BigDecimal ask = new BigDecimal(cursor.getString(0));
                        BigDecimal bid = new BigDecimal(cursor.getString(1));
                        BigDecimal askNew = new BigDecimal(c.getAsk());
                        BigDecimal bidNew = new BigDecimal(c.getBid());
                        if (askNew.compareTo(ask) >= 0 || cursor.getString(0).isEmpty()) {
                            c.setAskFluctuation(1);
                        } else {
                            c.setAskFluctuation(0);
                        }
                        if (bidNew.compareTo(bid) >= 0 || cursor.getString(1).isEmpty()) {
                            c.setBidFluctuation(1);
                        } else {
                            c.setBidFluctuation(0);
                        }
                    }
                } catch (Exception e) {
                    c.setAskFluctuation(1);
                    c.setBidFluctuation(1);
                }
                finally {
                    cursor.close();
                }
            }
        }
        db.delete(DbOpenHelper.COURSE_TABLE, null, null);
        for (OrganizationJson org : _list) {
            for (Course c : org.getCourses()) {
                ContentValues values = new ContentValues();
                values.put(DbOpenHelper.FLUCTUATION_ASK, c.getAskFluctuation());
                values.put(DbOpenHelper.FLUCTUATION_BID, c.getBidFluctuation());
                values.put(DbOpenHelper.CURRENCY_KEY, c.getCurrency().getAbbr());
                values.put(DbOpenHelper.ORGANIZATION_KEY, org.getId());
                values.put(DbOpenHelper.ASK, c.getAsk());
                values.put(DbOpenHelper.BID, c.getBid());
                db.insert(DbOpenHelper.COURSE_TABLE, null, values);
            }
        }
        db.close();
    }

    public void stopAlarmService() {
        Intent background = new Intent(mContext, AlarmService.class);
        mContext.stopService(background);
    }

}
