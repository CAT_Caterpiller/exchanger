package ua.com.thinkmobiles.exchanger.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import ua.com.thinkmobiles.exchanger.activities.MainActivity;
import ua.com.thinkmobiles.exchanger.R;

/**
 * Created by Admin on 17.09.2015.
 */
public class NotificationUtil {
    private NotificationUtil() {}

    public static Notification sendNotification(Context _context) {
        Intent intent = new Intent(_context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(_context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(_context)
                .setSmallIcon(R.drawable.ic_notify)
                .setContentTitle("Download")
                .setContentText("Download in progress")
                .setTicker("")
                .setSubText("")
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());

        return builder.build();
    }
}
