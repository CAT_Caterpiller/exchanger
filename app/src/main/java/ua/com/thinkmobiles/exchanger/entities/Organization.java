package ua.com.thinkmobiles.exchanger.entities;

import java.util.List;

/**
 * Created by Admin on 14.09.2015.
 */
public class Organization {
    private int mId;
    private String mOrgId;
    private String mOldId;
    private OrganizationType mOrgType;
    private String mTitle;
    private Region mRegion;
    private City mCity;
    private String phone;
    private String address;
    private String link;
    private List<Course> mCourses;

    public int getId() {
        return mId;
    }

    public void setId(int _id) {
        mId = _id;
    }

    public String getOrgId() {
        return mOrgId;
    }

    public void setOrgId(String _orgId) {
        mOrgId = _orgId;
    }

    public String getOldId() {
        return mOldId;
    }

    public void setOldId(String _oldId) {
        mOldId = _oldId;
    }

    public OrganizationType getOrgType() {
        return mOrgType;
    }

    public void setOrgType(OrganizationType _orgType) {
        mOrgType = _orgType;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String _title) {
        mTitle = _title;
    }

    public Region getRegion() {
        return mRegion;
    }

    public void setRegion(Region _region) {
        mRegion = _region;
    }

    public City getCity() {
        return mCity;
    }

    public void setCity(City _city) {
        mCity = _city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String _phone) {
        phone = _phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String _address) {
        address = _address;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String _link) {
        link = _link;
    }

    public List<Course> getCourses() {
        return mCourses;
    }

    public void setCourses(List<Course> _courses) {
        mCourses = _courses;
    }
}
