package ua.com.thinkmobiles.exchanger.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.List;

import ua.com.thinkmobiles.exchanger.activities.MainActivity;
import ua.com.thinkmobiles.exchanger.db.DbCurator;
import ua.com.thinkmobiles.exchanger.db.DbReader;
import ua.com.thinkmobiles.exchanger.entities.Organization;
import ua.com.thinkmobiles.exchanger.utils.NotificationUtil;

/**
 * Created by Admin on 16.09.2015.
 */
public class AlarmService extends Service {
    private DbCurator mDbCurator;
    private Context mContext;
    private boolean isRunning;
    private Thread mBackgroundThread;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        this.mContext = this;
        this.isRunning = false;
        this.mBackgroundThread = new Thread(myTask);
        mDbCurator = new DbCurator(mContext);
        //TODO create Notification
        startForeground(0, NotificationUtil.sendNotification(mContext));
    }

    private Runnable myTask = new Runnable() {
        public void run() {
            // Do something here
            mDbCurator.performAllOperations();
            stopSelf();
        }
    };

    @Override
    public void onDestroy() {
        this.isRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!this.isRunning) {
            this.isRunning = true;
            this.mBackgroundThread.start();
        }
        return START_STICKY;
    }

    @Override
    public boolean stopService(Intent name) {
        Log.d(MainActivity.TAG, "service finish");
        return super.stopService(name);
    }
}
