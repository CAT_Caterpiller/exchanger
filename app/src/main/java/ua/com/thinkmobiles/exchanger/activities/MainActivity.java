package ua.com.thinkmobiles.exchanger.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;

import java.util.List;

import ua.com.thinkmobiles.exchanger.R;
import ua.com.thinkmobiles.exchanger.adapters.OrgAdapter;
import ua.com.thinkmobiles.exchanger.db.DbReader;
import ua.com.thinkmobiles.exchanger.entities.Organization;
import ua.com.thinkmobiles.exchanger.utils.AlarmUtil;

/**
 * Created by Admin on 14.09.2015.
 */
public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, SwipeRefreshLayout.OnRefreshListener{
    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefresher;
    private LinearLayoutManager mLayoutManager;
    private List<Organization> mOrganizations;
    private DbReader mReader;
    private OrgAdapter mAdapter;

    public static final String TAG = "debug";
    public static final String URL = "http://resources.finance.ua/ru/public/currency-cash.json";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AlarmUtil.startAlert(getApplicationContext());

        mToolbar = (Toolbar) findViewById(R.id.toolbar_MA);
        setSupportActionBar(mToolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_MA);
        mSwipeRefresher = (SwipeRefreshLayout) findViewById(R.id.recyclerContainer_MA);
        mSwipeRefresher.setOnRefreshListener(this);

        mReader = new DbReader(getApplicationContext());
        do  {
            mOrganizations = mReader.readDataOrganizations();
        } while(!mReader.checkDataBase());

        mAdapter = new OrgAdapter(mOrganizations);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(TextUtils.isEmpty(newText)) {
            mAdapter.getFilter().filter("");
        } else {
            mAdapter.getFilter().filter(newText);
        }
        return true;
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefresher.setRefreshing(false);
                mOrganizations = mReader.readDataOrganizations();
                mAdapter.notifyDataSetChanged();
            }
        }, 1500);
    }
}
