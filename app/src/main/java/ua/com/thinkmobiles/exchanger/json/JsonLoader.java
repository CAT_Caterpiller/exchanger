package ua.com.thinkmobiles.exchanger.json;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ua.com.thinkmobiles.exchanger.activities.MainActivity;
import ua.com.thinkmobiles.exchanger.entities.Course;
import ua.com.thinkmobiles.exchanger.entities.Currency;

/**
 * Created by Admin on 14.09.2015.
 */
public class JsonLoader {
    private Context mContext;

    private final String ORG_TYPES = "orgTypes";
    private final String CURRENCIES = "currencies";
    private final String REGIONS = "regions";
    private final String CITIES = "cities";
    private final String ORGANIZATIONS = "organizations";
    private final String DATE = "date";

    private String mJson;

    public JsonLoader(Context _context) {
        this.mContext = _context;
    }

    public void receivingJson() throws IOException {
        URL url = new URL(MainActivity.URL);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(10000);
        conn.setRequestMethod("GET");

        int respCode = conn.getResponseCode();
        if (respCode == 200) {
            final BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            mJson = sb.toString();
        }
    }

    public boolean checkJson() {
        if(mJson.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public CuratorOfMaps getAllMaps() throws JSONException {
        CuratorOfMaps marker = new CuratorOfMaps();
        marker.setAllTypes(getMap(ORG_TYPES));
        marker.setAllCurrencies(getMap(CURRENCIES));
        marker.setAllRegions(getMap(REGIONS));
        marker.setAllCities(getMap(CITIES));
        return marker;
    }

    private Map<String, String> getMap(String _jsonKey) throws JSONException {
        JSONObject jsonObject = new JSONObject(mJson);
        GsonBuilder builder = new GsonBuilder();
        Gson gson           = builder.create();

        Map<String, String> map = new HashMap<>();
        map = (Map<String,String>) gson.fromJson(String.valueOf(jsonObject.getJSONObject(_jsonKey)), map.getClass());
        return map;
    }

    public List<OrganizationJson> getAllOrganizationsJson(CuratorOfMaps _marker) throws JSONException {
        JSONObject jsonObject = new JSONObject(mJson);
        GsonBuilder builder = new GsonBuilder();
        Gson gson           = builder.create();
        List<OrganizationJson> list = new ArrayList<>();

        JSONArray orgArray = jsonObject.getJSONArray(ORGANIZATIONS);
        for(int i = 0; i < orgArray.length(); i++) {
            OrganizationJson organization = gson.fromJson(String.valueOf(orgArray.getJSONObject(i)), OrganizationJson.class);
            JSONObject curr = orgArray.getJSONObject(i).getJSONObject(CURRENCIES);
            if(organization == null) {
                Log.d(MainActivity.TAG, "error");
            }
            List<Course> courses = new ArrayList<>();
            for(String fox : _marker.getAllCurrencies().keySet()) {
                if (curr.has(fox)) {
                    Course course = gson.fromJson(String.valueOf(curr.getJSONObject(fox)), Course.class);
                    course.setCurrency(new Currency(fox, _marker.getAllCurrencies().get(fox)));
                    Log.d(MainActivity.TAG, fox);
                    Log.d(MainActivity.TAG, course.getAsk());
                    courses.add(course);
                }
            }
            organization.setCourses(courses);
            list.add(organization);

        }
        return list;
    }

    public String getDate() throws JSONException{
        JSONObject object = new JSONObject(mJson);
        return object.getString(DATE);
    }

}
