package ua.com.thinkmobiles.exchanger.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import ua.com.thinkmobiles.exchanger.activities.MapActivity;
import ua.com.thinkmobiles.exchanger.activities.OrganizationActivity;

/**
 * Created by Admin on 19.09.2015.
 */
public class TabClickUtil {
    public static final String ORGANIZATION = "organization";
    public static final String CITY = "city";
    public static final String ADDRESS = "address";
    public static final String KEY_ORG = "key";

    private TabClickUtil() {}

    public static boolean validateText(String text) {
        if(TextUtils.isEmpty(text)) {
            return false;
        } else {
            return true;
        }
    }

    public static void callPhone(Context _context, String phone) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(intent);
    }

    public static void moveToLink(Context _context, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(link));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(intent);
    }

    public static void moveToMap(Context _context, String _organiz, String _city, String _address) {
        Intent intent = new Intent(_context, MapActivity.class);
        intent.putExtra(ORGANIZATION, _organiz);
        intent.putExtra(CITY, _city);
        intent.putExtra(ADDRESS, _address);
        _context.startActivity(intent);
    }

    public static void moveToOrgActivity(Context _context, String _key) {
        Intent intent = new Intent(_context, OrganizationActivity.class);
        intent.putExtra(KEY_ORG, _key);
        _context.startActivity(intent);
    }
}
