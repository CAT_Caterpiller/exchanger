package ua.com.thinkmobiles.exchanger.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import ua.com.thinkmobiles.exchanger.services.AlarmService;

/**
 * Created by Admin on 16.09.2015.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent background = new Intent(context, AlarmService.class);
        context.startService(background);
    }
}
