package ua.com.thinkmobiles.exchanger.json;

import java.util.List;

import ua.com.thinkmobiles.exchanger.entities.Course;
import ua.com.thinkmobiles.exchanger.entities.Currency;

/**
 * Created by Admin on 15.09.2015.
 */
public class OrganizationJson {
    private String id;
    private String oldId;
    private String orgType;
    private String title;
    private String regionId;
    private String cityId;
    private String phone;
    private String address;
    private String link;
    private List<Course> mCourses;

    public String getId() {
        return id;
    }

    public void setId(String _id) {
        id = _id;
    }

    public String getOldId() {
        return oldId;
    }

    public void setOldId(String _oldId) {
        oldId = _oldId;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String _orgType) {
        orgType = _orgType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String _title) {
        title = _title;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String _regionId) {
        regionId = _regionId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String _cityId) {
        cityId = _cityId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String _phone) {
        phone = _phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String _address) {
        address = _address;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String _link) {
        link = _link;
    }

    public List<Course> getCourses() {
        return mCourses;
    }

    public void setCourses(List<Course> _courses) {
        mCourses = _courses;
    }
}
