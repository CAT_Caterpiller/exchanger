package ua.com.thinkmobiles.exchanger.adapters;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import ua.com.thinkmobiles.exchanger.utils.TabClickUtil;
import ua.com.thinkmobiles.exchanger.utils.TextfieldsUtil;
import ua.com.thinkmobiles.exchanger.views.OrganizationCard;
import ua.com.thinkmobiles.exchanger.R;
import ua.com.thinkmobiles.exchanger.entities.Organization;

/**
 * Created by Admin on 18.09.2015.
 */
public class OrgAdapter extends RecyclerView.Adapter<OrgAdapter.MyViewHolder> implements Filterable {
    private List<Organization> mOrganizationList;
    private List<Organization> mFilterList;
    private Context mContext;

    public OrgAdapter(List<Organization> _list) {
        mOrganizationList = _list;
    }

    @Override
    public OrgAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_container, parent, false);
        mContext = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrgAdapter.MyViewHolder holder, int position) {
        Organization organization = mOrganizationList.get(position);
        holder.mOrganization = organization;
        holder.mCard.getTitle().setText(organization.getTitle());
        holder.mCard.getRegion().setText(organization.getRegion().getName());
        holder.mCard.getCity().setText(organization.getCity().getName());
        holder.mCard.getPhone().setText(TextfieldsUtil.getPhoneField(organization.getPhone()));
        holder.mCard.getAddress().setText(TextfieldsUtil.getAddressField(organization.getAddress()));
    }

    @Override
    public int getItemCount() {
        return mOrganizationList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements TabLayout.OnTabSelectedListener{
        protected OrganizationCard mCard;
        protected Organization mOrganization;

        public MyViewHolder(View _view) {
            super(_view);
            mCard = (OrganizationCard) _view.findViewById(R.id.orgView);
            mCard.getTabLayout().setOnTabSelectedListener(this);
        }

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            switch (tab.getPosition()) {
                case 0:
                    if(TabClickUtil.validateText(mOrganization.getLink())) {
                        TabClickUtil.moveToLink(mContext, mOrganization.getLink());
                    }
                    break;
                case 1:
                    TabClickUtil.moveToMap(mContext, mOrganization.getTitle(),
                            mOrganization.getCity().getName(), mOrganization.getAddress());
                    break;
                case 2:
                    if(TabClickUtil.validateText(mOrganization.getPhone())) {
                        TabClickUtil.callPhone(mContext, mOrganization.getPhone());
                    }
                    break;
                case 3:
                    TabClickUtil.moveToOrgActivity(mContext, mOrganization.getOrgId());
                    break;
            }

        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            switch (tab.getPosition()) {
                case 0:
                    if(TabClickUtil.validateText(mOrganization.getLink())) {
                        TabClickUtil.moveToLink(mContext, mOrganization.getLink());
                    }
                    break;
                case 1:
                    TabClickUtil.moveToMap(mContext, mOrganization.getTitle(),
                            mOrganization.getCity().getName(), mOrganization.getAddress());
                    break;
                case 2:
                    if(TabClickUtil.validateText(mOrganization.getPhone())) {
                        TabClickUtil.callPhone(mContext, mOrganization.getPhone());
                    }
                    break;
                case 3:
                    TabClickUtil.moveToOrgActivity(mContext, mOrganization.getOrgId());
                    break;
            }
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                final List<Organization> result = new ArrayList<>();
                if (mFilterList == null) {
                    mFilterList = mOrganizationList;
                }
                if (constraint != null) {
                    if (mFilterList != null & mFilterList.size() > 0) {
                        for (final Organization org : mFilterList) {
                            if (org.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())
                                    || org.getRegion().getName().toLowerCase().contains(constraint.toString().toLowerCase())
                                    || org.getCity().getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                result.add(org);
                            }
                        }
                    }
                    filterResults.values = result;
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mOrganizationList = (ArrayList<Organization>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
