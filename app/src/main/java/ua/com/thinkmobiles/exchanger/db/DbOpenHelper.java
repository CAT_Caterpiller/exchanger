package ua.com.thinkmobiles.exchanger.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Admin on 15.09.2015.
 */
public class DbOpenHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "exchanger.db";
    private static final int VERSION = 1;

    public static final String CURRENCY_TABLE = "currency";
    public static final String COURSE_TABLE = "course";
    public static final String ORGTYPE_TABLE = "orgtype";
    public static final String REGION_TABLE = "region";
    public static final String CITY_TABLE = "city";
    public static final String ORGANIZATION_TABLE = "organization";

    public static final String ID = "_id";
    public static final String ID_KEY = "id_key";
    public static final String OLDID_KEY = "old_id";
    public static final String ORGTYPE_KEY = "orgtype_key";
    public static final String TITLE_KEY = "title";
    public static final String REGION_KEY = "region_key";
    public static final String CITY_KEY = "city_key";
    public static final String PHONE_KEY = "phone";
    public static final String ADDRESS_KEY = "address";
    public static final String LINK_KEY = "link";
    public static final String VALUE_KEY = "value";
    public static final String CURRENCY_KEY = "currency_key";
    public static final String ORGANIZATION_KEY = "organization_key";
    public static final String ASK = "ask";
    public static final String BID = "bid";
    public static final String FLUCTUATION_ASK = "fluctuation_ask";
    public static final String FLUCTUATION_BID = "fluctuation_bid";

    public static DbOpenHelper sInstance;

    public DbOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    public static DbOpenHelper getInstance(Context _context) {
        if(sInstance == null) {
            sInstance = new DbOpenHelper(_context);
            return sInstance;
        } else {
            return sInstance;
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ORGANIZATION_TABLE = "CREATE TABLE " + ORGANIZATION_TABLE + "("
                + ID + " INTEGER PRIMARY KEY autoincrement,"
                + ID_KEY + " TEXT," + OLDID_KEY + " TEXT,"
                + ORGTYPE_KEY + " TEXT,"
                + TITLE_KEY + " TEXT," + PHONE_KEY + " TEXT," + ADDRESS_KEY + " TEXT," + LINK_KEY +" TEXT," + REGION_KEY +" TEXT," + CITY_KEY + " TEXT,"
                + "FOREIGN KEY (" + ORGTYPE_KEY + ")REFERENCES "+ ORGTYPE_TABLE + "("+ID+")"
                + "FOREIGN KEY (" + REGION_KEY + ")REFERENCES "+ REGION_TABLE + "("+ID+")"
                + "FOREIGN KEY (" + CITY_KEY + ")REFERENCES "+ CITY_TABLE + "("+ID+")" +")";

        db.execSQL(CREATE_ORGANIZATION_TABLE);

        String CREATE_REGION_TABLE = "CREATE TABLE " + REGION_TABLE
                + "(" + ID + " TEXT PRIMARY KEY ON CONFLICT REPLACE," + VALUE_KEY + " TEXT"+ ")";
        db.execSQL(CREATE_REGION_TABLE);

        String CREATE_CITY_TABLE = "CREATE TABLE " + CITY_TABLE
                + "(" + ID + " TEXT PRIMARY KEY ON CONFLICT REPLACE," + VALUE_KEY + " TEXT"+ ")";
        db.execSQL(CREATE_CITY_TABLE);

        String CREATE_ORGTYPE_TABLE = "CREATE TABLE " + ORGTYPE_TABLE
                + "(" + ID + " TEXT PRIMARY KEY ON CONFLICT REPLACE," + VALUE_KEY + " TEXT"+ ")";
        db.execSQL(CREATE_ORGTYPE_TABLE);

        String CREATE_CURRENCY_TABLE = "CREATE TABLE " + CURRENCY_TABLE
                + "(" + ID + " TEXT PRIMARY KEY ON CONFLICT REPLACE," + VALUE_KEY + " TEXT"+ ")";
        db.execSQL(CREATE_CURRENCY_TABLE);

        String CREATE_CURSE_TABLE = "CREATE TABLE " + COURSE_TABLE
                + "(" + CURRENCY_KEY + " TEXT," + ORGANIZATION_KEY + " TEXT,"
                + ASK + " TEXT," + BID + " TEXT," + FLUCTUATION_ASK + " INTEGER,"
                + FLUCTUATION_BID + " INTEGER,"
                + "FOREIGN KEY (" + CURRENCY_KEY + ")REFERENCES "+ CURRENCY_TABLE + "("+ID+")"
                + "FOREIGN KEY (" + ORGANIZATION_KEY + ")REFERENCES "+ ORGANIZATION_TABLE + "("+ID_KEY+")"
                + "UNIQUE(" + CURRENCY_KEY + "," + ORGANIZATION_KEY + ") ON CONFLICT REPLACE)";
        db.execSQL(CREATE_CURSE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ORGANIZATION_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + ORGTYPE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + REGION_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CITY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CURRENCY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + COURSE_TABLE);
        onCreate(db);
    }
}
