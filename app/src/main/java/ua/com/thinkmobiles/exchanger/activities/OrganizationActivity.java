package ua.com.thinkmobiles.exchanger.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.List;

import ua.com.thinkmobiles.exchanger.R;
import ua.com.thinkmobiles.exchanger.db.DbReader;
import ua.com.thinkmobiles.exchanger.entities.Course;
import ua.com.thinkmobiles.exchanger.entities.Organization;
import ua.com.thinkmobiles.exchanger.utils.TabClickUtil;
import ua.com.thinkmobiles.exchanger.views.CourseCard;

/**
 * Created by Admin on 20.09.2015.
 */
public class OrganizationActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private DbReader mReader;
    private Organization mOrganization;

    private TextView mTvTitle, mTvLink, mTvAddress, mTvPhone;
    private FrameLayout mFrameLayout;
    private LinearLayout mLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization);

        initOrganization(getKey());
        initFields();
        prepeareFab();
        fillingFields();
        createListCurrency();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.org_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.share) {
            //TODO create the dialog
        }
        return super.onOptionsItemSelected(item);
    }

    private String getKey() {
        Intent intent = getIntent();
        return intent.getStringExtra(TabClickUtil.KEY_ORG);
    }

    private void initOrganization(String key) {
        mReader = new DbReader(this);
        mOrganization = mReader.readOrganization(key);
    }

    private void initFields() {
        mTvTitle = (TextView) findViewById(R.id.tvTitle_OA);
        mTvLink = (TextView) findViewById(R.id.tvLink_OA);
        mTvAddress = (TextView) findViewById(R.id.tvAddress_OA);
        mTvPhone = (TextView) findViewById(R.id.tvPhone_OA);
        mLinearLayout = (LinearLayout) findViewById(R.id.listView_OA);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_OA);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow);
        mToolbar.setTitle(mOrganization.getTitle());
        mToolbar.setSubtitle(mOrganization.getCity().getName());
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mFrameLayout = (FrameLayout) findViewById(R.id.flLayout_OA);
    }

    private void fillingFields() {
        mTvTitle.setText(mOrganization.getTitle());
        mTvLink.setText(mOrganization.getLink());
        mTvLink.setPaintFlags(mTvLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTvAddress.setText(validateText(mOrganization.getAddress()));
        mTvPhone.setText(validateText(mOrganization.getPhone()));
    }

    private String validateText(String _text) {
        if (TextUtils.isEmpty(_text)) {
            return " -----";
        } else {
            return _text;
        }
    }

    private void createListCurrency() {
        List<Course> courses = mOrganization.getCourses();
        mLinearLayout = (LinearLayout) findViewById(R.id.listView_OA);
        for (int i = 0; i < courses.size(); i++) {
            CourseCard courseCard = new CourseCard(this);
            courseCard.createCurrencyCard(courses.get(i));
            mLinearLayout.addView(courseCard);
        }
    }

    private void prepeareFab() {
        final FloatingActionsMenu menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.action_OA);

        final FloatingActionButton actionA = (FloatingActionButton) findViewById(R.id.actionA_OA);
        final FloatingActionButton actionB = (FloatingActionButton) findViewById(R.id.actionB_OA);
        final FloatingActionButton actionC = (FloatingActionButton) findViewById(R.id.actionC_OA);

        menuMultipleActions.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                mFrameLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuCollapsed() {
                mFrameLayout.setVisibility(View.INVISIBLE);
            }
        });

        connectFabListeners(actionA, actionB, actionC);
    }

    private void connectFabListeners(FloatingActionButton a, FloatingActionButton b, FloatingActionButton c) {
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TabClickUtil.moveToMap(OrganizationActivity.this, mOrganization.getTitle(), mOrganization.getCity().getName(), mOrganization.getAddress());
            }
        });
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TabClickUtil.validateText(mOrganization.getLink())) {
                    TabClickUtil.moveToLink(OrganizationActivity.this, mOrganization.getLink());
                }
            }
        });
        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TabClickUtil.validateText(mOrganization.getPhone())) {
                    TabClickUtil.callPhone(OrganizationActivity.this, mOrganization.getPhone());
                }
            }
        });
    }
}
